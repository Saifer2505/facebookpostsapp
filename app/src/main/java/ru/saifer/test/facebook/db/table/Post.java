package ru.saifer.test.facebook.db.table;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ru.saifer.test.facebook.api.models.AttachmentModel;
import ru.saifer.test.facebook.api.models.ImageModel;
import ru.saifer.test.facebook.api.models.MediaModel;
import ru.saifer.test.facebook.api.models.PostModel;
import ru.saifer.test.facebook.api.models.ProfileModel;
import ru.saifer.test.facebook.api.models.core.Data;
import ru.saifer.test.facebook.db.DBFactory;
import ru.saifer.test.facebook.db.core.DataBaseModel;

@Table(name="Posts", id = "_id")
public class Post extends DataBaseModel {

    public static final Parcelable.Creator<Post> CREATOR = new Parcelable.Creator<Post>() {
        public Post createFromParcel(Parcel in) {
            return new Post(in);
        }
        public Post[] newArray(int size) {
            return new Post[size];
        }
    };

    @Column(name = "created_time")
    public Date createdTime;
    @Column(name = "updated_time")
    public Date updatedTime;
    @Column(name = "profile_id")
    public Profile from;
    @Column
    public String message;
    @Column
    public String picture;
    @Column(name = "full_picture")
    public String fullPicture;
    @Column
    public String type;

    public Post() {
        super();
    }

    private Post(Parcel paramParcel) {
        super(paramParcel);
        createdTime = new Date(paramParcel.readLong());
        updatedTime = new Date(paramParcel.readLong());
        from = paramParcel.readParcelable(Profile.class.getClassLoader());
        message = paramParcel.readString();
        picture = paramParcel.readString();
        fullPicture = paramParcel.readString();
        type = paramParcel.readString();
    }

    public void writeToParcel(Parcel paramParcel, int paramInt) {
        super.writeToParcel(paramParcel, paramInt);
        paramParcel.writeLong(createdTime != null ? createdTime.getTime() : -1L);
        paramParcel.writeLong(updatedTime != null ? updatedTime.getTime() : -1L);
        paramParcel.writeParcelable(from, paramInt);
        paramParcel.writeString(message);
        paramParcel.writeString(picture);
        paramParcel.writeString(fullPicture);
        paramParcel.writeString(type);
    }

    public int describeContents() {
        return 0;
    }

    @NonNull
    public PostModel toPostModel() {

        Profile profile = this.from;
        ProfileModel profileModel = new ProfileModel();
        profileModel.setId(profile.id);
        profileModel.setName(profile.name);

        PostModel postModel = new PostModel();
        postModel.setId(this.id);
        postModel.setCreatedTime(this.createdTime);
        postModel.setUpdatedTime(this.updatedTime);
        postModel.setFrom(profileModel);
        postModel.setMessage(this.message);
        postModel.setPicture(this.picture);
        postModel.setFullPicture(this.fullPicture);
        postModel.setType(this.type);

        List<Attachment> attachmentList = DBFactory.getAttachmentListByPost(this);
        if (!attachmentList.isEmpty()) {

            List<AttachmentModel> attachmentModelList = new ArrayList<>(attachmentList.size());
            for (Attachment attachment: attachmentList) {
                AttachmentModel attachmentModel = new AttachmentModel();
                attachmentModel.setDescription(attachment.description);

                Media media = attachment.media;
                if (media != null) {
                    MediaModel mediaModel = new MediaModel();

                    Image image = media.image;
                    if (image != null) {
                        ImageModel imageModel = new ImageModel();
                        imageModel.setSrc(image.src);
                        imageModel.setWidth(image.width);
                        imageModel.setHeight(image.height);
                        mediaModel.setImage(imageModel);
                    }
                    attachmentModel.setMedia(mediaModel);
                }

                attachmentModel.setType(attachment.type);
                attachmentModel.setUrl(attachment.url);
                attachmentModelList.add(attachmentModel);
            }
            postModel.setAttachments(new Data<>(attachmentModelList));
        }
        return postModel;
    }

    @Override
    public String toString() {
        return "["+
                "id=" + id +
                ", createdTime=" + createdTime.getTime() +
                ", updatedTime=" + updatedTime.getTime() +
                ", from=" + from +
                ", message=" + message +
                ", picture=" + picture +
                ", fullPicture=" + fullPicture +
                ", type=" + type +
                "]";
    }
}
