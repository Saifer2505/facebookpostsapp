package ru.saifer.test.facebook.api.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class MediaModel implements Serializable {

    @SerializedName("image")
    @Expose
    private ImageModel image;

    public ImageModel getImage() {
        return image;
    }
    public void setImage(ImageModel image) {
        this.image = image;
    }
}
