package ru.saifer.test.facebook.api.models.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Struct {
 *   id: numeric string,
 *   post_id: string,
 *   success: boolean
 * }
 */
public class Struct {

    @SerializedName("id")
    @Expose
    private String id;
    //@SerializedName("post_id")
    //@Expose
    //private String postId;
    @SerializedName("success")
    @Expose
    private Boolean success;

    public String getId() {
        return id;
    }

    /*public String getPostId() {
        return postId;
    }*/

    public Boolean isSuccess() {
        return success;
    }

}
