package ru.saifer.test.facebook.db.core;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;

public abstract class DataBaseModel extends Model implements Parcelable {

    @Column(name="id", unique=true/*, onUniqueConflict = Column.ConflictAction.REPLACE*/)
    @NonNull
    public String id;

    public DataBaseModel() {}

    protected DataBaseModel(Parcel paramParcel) {
        id = paramParcel.readString();
    }

    public void writeToParcel(Parcel paramParcel, int paramInt) {
        paramParcel.writeString(id);
    }

}
