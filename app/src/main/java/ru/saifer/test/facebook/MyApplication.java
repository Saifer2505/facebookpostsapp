package ru.saifer.test.facebook;

import android.support.multidex.MultiDexApplication;

import com.activeandroid.ActiveAndroid;

public class MyApplication extends MultiDexApplication {

    @Override
    public void onCreate() {
        super.onCreate();

        ActiveAndroid.initialize(this, BuildConfig.DEBUG);
    }
}
