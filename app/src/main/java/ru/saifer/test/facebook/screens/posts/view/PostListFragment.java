package ru.saifer.test.facebook.screens.posts.view;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import ru.saifer.test.facebook.MainActivity;
import ru.saifer.test.facebook.R;
import ru.saifer.test.facebook.adapters.HidingScrollListener;
import ru.saifer.test.facebook.api.models.PostModel;
import ru.saifer.test.facebook.screens.posts.adapter.PostListAdapter;
import ru.saifer.test.facebook.screens.posts.presenter.PostListPresenterImpl;

public class PostListFragment extends Fragment implements PostListView, View.OnClickListener {

    private static final String ACTION_UPDATE = PostListFragment.class.getSimpleName().concat(".ACTION_UPDATE");
    private static final String EXTRA_POST = "post";

    private PostListPresenterImpl mPresenter;
    private View mAddPostButton;
    private ProgressBar mProgress;
    private SwipeRefreshLayout mRefreshLayout;
    private PostListAdapter mPostListAdapter;
    private RecyclerView mRecyclerView;
    private TextView mErrorLabel;

    BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (ACTION_UPDATE.equals(intent.getAction())) {

                if (intent.hasExtra(EXTRA_POST)) {
                    mPostListAdapter.addOrUpdateItem((PostModel) intent.getSerializableExtra(EXTRA_POST));
                    return;
                }

                loadData();
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.post_list, container, false);

        mProgress = (ProgressBar)view.findViewById(R.id.post_list_progress);

        mAddPostButton = getActivity().findViewById(R.id.fab_add_post);
        mAddPostButton.setVisibility(View.VISIBLE);
        mAddPostButton.setOnClickListener(this);

        mErrorLabel = (TextView)view.findViewById(R.id.error_label);
        mErrorLabel.setVisibility(View.INVISIBLE);

        mPostListAdapter = new PostListAdapter(getActivity());
        mPostListAdapter.getOnClickItemObservable()
                .subscribe(postModel -> MainActivity.startNewPostDialogFragment(getActivity(), postModel));

        mRecyclerView = (RecyclerView)view.findViewById(R.id.post_list);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(mPostListAdapter);
        mRecyclerView.addOnScrollListener(new HidingScrollListener() {
            @Override
            public void onHide() {
                hideViews();
            }
            @Override
            public void onShow() {
                showViews();
            }
        });

        mRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.refresh);
        mRefreshLayout.setOnRefreshListener(() -> loadData());

        ItemTouchHelper swipeToDismissTouchHelper = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(
                ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT
        ) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }
            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                deletePost(viewHolder.getAdapterPosition());
            }
        });
        swipeToDismissTouchHelper.attachToRecyclerView(mRecyclerView);

        mPresenter = new PostListPresenterImpl(getActivity(), this);

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ACTION_UPDATE);
        getActivity().registerReceiver(mReceiver, intentFilter);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadData();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.post_list, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_logout:
                mPresenter.logout();
                return true;

            case R.id.menu_clear_db:
                mPostListAdapter.clear();
                mPresenter.clearPosts();
                showViews();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fab_add_post:
                MainActivity.startNewPostDialogFragment(getActivity());
        }
    }

    @Override
    public void onDestroyView() {
        if (mPresenter != null) {
            mPresenter.destroy();
            mPresenter = null;
        }
        if (mReceiver != null) {
            getActivity().unregisterReceiver(mReceiver);
            mReceiver = null;
        }
        super.onDestroyView();
    }

    @Override
    public void showProgress() {
        mProgress.setVisibility(View.VISIBLE);
        mErrorLabel.setVisibility(View.INVISIBLE);
        if (mRefreshLayout.isRefreshing()) {
            mRefreshLayout.setRefreshing(false);
        }
        mRefreshLayout.setEnabled(false);
    }

    @Override
    public void hideProgress() {
        mProgress.setVisibility(View.INVISIBLE);
        mErrorLabel.setVisibility(mPostListAdapter.isEmpty() ? View.VISIBLE : View.INVISIBLE);
        mRefreshLayout.setEnabled(true);
    }

    @Override
    public void onErrorMessage(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();

        mErrorLabel.setVisibility(mPostListAdapter.isEmpty() ? View.VISIBLE : View.INVISIBLE);
        if (mRefreshLayout.isRefreshing()) {
            mRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    @Nullable
    public RecyclerView getRecyclerView() {
        return mRecyclerView;
    }

    @Override
    public void onPostData(@Nullable List<PostModel> data, boolean isOffset) {
        if (mRefreshLayout.isRefreshing()) {
            mRefreshLayout.setRefreshing(false);
        }
        mPostListAdapter.addItems(data, isOffset);
        mErrorLabel.setVisibility(mPostListAdapter.isEmpty() ? View.VISIBLE : View.INVISIBLE);
    }

    @Override
    public void onDeletePostFailed(int position, @NonNull PostModel postModel) {
        mPostListAdapter.addItem(position, postModel);
        mErrorLabel.setVisibility(mPostListAdapter.isEmpty() ? View.VISIBLE : View.INVISIBLE);
    }

    @Override
    public void onSetClickListenerByLogoutDialog(@NonNull DialogInterface.OnClickListener listener) {
        new AlertDialog.Builder(getActivity())
                .setTitle(R.string.facebook_title)
                .setMessage(R.string.logout_dialog_message)
                .setPositiveButton(R.string.logout_label, listener)
                .setNegativeButton(R.string.cancel_label, null)
                .show();
    }

    @Override
    public void onLogout() {
        MainActivity.logout(getActivity());

        mAddPostButton.setVisibility(View.INVISIBLE);
        getFragmentManager().beginTransaction().remove(this).commit();
    }

    private void loadData() {
        if (mPostListAdapter.isEmpty()) {
            mPresenter.getPosts();
        } else {
            mPresenter.syncPosts();
        }
    }

    private void deletePost(int position) {

        final PostModel postModel = mPostListAdapter.removeItem(position);
        mErrorLabel.setVisibility(mPostListAdapter.isEmpty() ? View.VISIBLE : View.INVISIBLE);

        Snackbar.make(mAddPostButton, R.string.delete_label, Snackbar.LENGTH_LONG)
                .addCallback(new Snackbar.Callback() {
                    @Override
                    public void onDismissed(Snackbar snackbar, int event) {
                        super.onDismissed(snackbar, event);

                        if (event == DISMISS_EVENT_TIMEOUT) {
                            mPresenter.deletePost(position, postModel);
                        }
                    }
                })
                .setAction(R.string.cancel_label, view -> onDeletePostFailed(position, postModel))
                .show();
    }

    private void hideViews() {
        ViewGroup.LayoutParams lp = mAddPostButton.getLayoutParams();
        int fabBottomMargin = lp instanceof CoordinatorLayout.LayoutParams ? ((CoordinatorLayout.LayoutParams)lp).bottomMargin : 0;
        mAddPostButton.animate().translationY(mAddPostButton.getHeight()+fabBottomMargin).setInterpolator(new AccelerateInterpolator(2)).start();
    }

    private void showViews() {
        mAddPostButton.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2)).start();
    }

    public static void sendActionUpdate(@NonNull Context context, @Nullable PostModel postModel) {
        Intent intent = new Intent(PostListFragment.ACTION_UPDATE);
        if (postModel != null) {
            intent.putExtra(EXTRA_POST, postModel);
        }
        context.sendBroadcast(intent);
    }

}
