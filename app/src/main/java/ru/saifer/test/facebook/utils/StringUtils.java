package ru.saifer.test.facebook.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public final class StringUtils {

    private static SimpleDateFormat mDateFormat;

    public static String parseDate(Date date, String pattern) {
        if (mDateFormat == null) {
            mDateFormat = new SimpleDateFormat(pattern, Locale.US);
        } else {
            mDateFormat.applyPattern(pattern);
        }
        return mDateFormat.format(date);
    }

}
