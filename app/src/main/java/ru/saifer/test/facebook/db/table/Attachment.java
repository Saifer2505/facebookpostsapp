package ru.saifer.test.facebook.db.table;

import android.os.Parcel;
import android.os.Parcelable;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

@Table(name="Attachements", id = "_id")
public class Attachment extends Model implements Parcelable {

    public static final Parcelable.Creator<Attachment> CREATOR = new Parcelable.Creator<Attachment>() {
        public Attachment createFromParcel(Parcel in) {
            return new Attachment(in);
        }
        public Attachment[] newArray(int size) {
            return new Attachment[size];
        }
    };

    @Column(name = "post_id")
    public Post post;
    @Column
    public String title;
    @Column
    public String description;
    @Column(name = "media_id")
    public Media media;
    @Column
    public String type;
    @Column
    public String url;

    public Attachment() {
    }

    private Attachment(Parcel paramParcel) {
        post = paramParcel.readParcelable(Post.class.getClassLoader());
        title = paramParcel.readString();
        description = paramParcel.readString();
        media = paramParcel.readParcelable(Media.class.getClassLoader());
        type = paramParcel.readString();
        url = paramParcel.readString();
    }

    public void writeToParcel(Parcel paramParcel, int paramInt) {
        paramParcel.writeParcelable(post, paramInt);
        paramParcel.writeString(title);
        paramParcel.writeString(description);
        paramParcel.writeParcelable(media, paramInt);
        paramParcel.writeString(type);
        paramParcel.writeString(url);
    }

    public int describeContents() {
        return 0;
    }

    @Override
    public String toString() {
        return "["+
                "post=" + post +
                ", title=" + title +
                ", description=" + description +
                ", media=" + media +
                ", type=" + type +
                ", url=" + url +
                "]";
    }
}
