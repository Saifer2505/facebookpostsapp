package ru.saifer.test.facebook.api;

import java.util.Map;

import okhttp3.MultipartBody;
import retrofit2.http.DELETE;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;
import ru.saifer.test.facebook.api.models.PostModel;
import ru.saifer.test.facebook.api.models.core.Data;
import ru.saifer.test.facebook.api.models.responses.Struct;
import rx.Observable;

public interface FacebookApi {

    String API_BASE_URL = "https://graph.facebook.com";

    /**
     * Reading posts method
     * @param url url
     * @return post model list
     */
    @GET
    Observable<Data<PostModel>> getPosts(@Url String url);

    /**
     * Reading posts method
     * @param query query
     * @return post list
     */
    @GET("/v2.8/me/feed")
    Observable<Data<PostModel>> getPosts(@QueryMap Map<String, String> query);

    /**
     * Reading post method
     * @param postId post id
     * @param query query
     * @return post model
     */
    @GET(value = "/v2.8/{post-id}")
    Observable<PostModel> getPost(@Path("post-id") String postId,
                                  @QueryMap Map<String, String> query);

    /**
     * Publishing post method
     * @param userId user id
     * @param params param post
     * @return result
     */
    @FormUrlEncoded
    @POST(value = "/v2.8/{user-id}/feed")
    Observable<Struct> publishPost(@Path("user-id") String userId,
                                   @FieldMap Map<String, String> params);

    /**
     * Publishing post method
     * @param params param post
     * @return result
     */
    @FormUrlEncoded
    @POST("/v2.8/me/feed")
    Observable<Struct> publishPost(@FieldMap Map<String, String> params);

    /**
     * Updating post method
     * @param postId post id
     * @param params param post
     * @return result
     */
    @FormUrlEncoded
    @POST(value = "/v2.8/{post-id}")
    Observable<Struct> updatePost(@Path("post-id") String postId,
                                  @FieldMap Map<String, String> params);

    /**
     * Deleting post method
     * @param postId post id
     * @param query param post
     * @return result
     */
    @DELETE(value = "/v2.8/{post-id}")
    Observable<Struct> deletePost(@Path("post-id") String postId,
                                  @QueryMap Map<String, String> query);

    /**
     * Upload photo method
     * @param objectId object id (post, ..)
     * @param accessToken token
     * @param published true, if published
     * @param file file name
     * @return result
     */
    @Multipart
    @POST(value = "/v2.8/{object-id}/photos")
    Observable<Struct> photo(@Path("object-id") String objectId,
                             @Part MultipartBody.Part accessToken,
                             @Part MultipartBody.Part published,
                             @Part MultipartBody.Part file);

    /**
     * Upload photo method
     * @param accessToken token
     * @param published true, if published
     * @param file file name
     * @return result
     */
    @Multipart
    @POST("/v2.8/me/photos")
    Observable<Struct> photo(@Part MultipartBody.Part accessToken,
                             @Part MultipartBody.Part published,
                             @Part MultipartBody.Part file);

}
