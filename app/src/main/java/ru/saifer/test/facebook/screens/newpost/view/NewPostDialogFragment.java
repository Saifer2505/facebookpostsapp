package ru.saifer.test.facebook.screens.newpost.view;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import ru.saifer.test.facebook.R;
import ru.saifer.test.facebook.api.models.PostModel;
import ru.saifer.test.facebook.api.models.requests.AttachedMedia;
import ru.saifer.test.facebook.api.models.requests.PostRequest;
import ru.saifer.test.facebook.screens.newpost.presenter.NewPostPresenterImpl;
import ru.saifer.test.facebook.screens.posts.view.PostListFragment;
import ru.saifer.test.facebook.utils.Utils;

public class NewPostDialogFragment extends DialogFragment implements NewPostView, View.OnClickListener {

    private static final String EXTRA_POST_ID = "id";
    private static final String EXTRA_POST_MESSAGE = "message";
    private static final String EXTRA_POST_IMAGE = "image";

    private static final int REQUEST_CODE_READ_EXTERNAL_STORAGE = 0x0010;
    private static final int REQUEST_CODE_SELECT_IMAGE_FILE = 0x0100;

    private NewPostPresenterImpl mPresenter;
    private List<AttachedMedia> mAttachedMediaList;
    private ProgressDialog mProgressDialog;
    private EditText mPostText;
    private ProgressBar mPostImageProgress;
    private TextView mPostImageLabel;
    private ImageView mPostImage;

    public static void start(Activity activity, PostModel postModel) {
        NewPostDialogFragment dialogFragment = new NewPostDialogFragment();

        if (postModel != null) {
            Bundle args = new Bundle();
            args.putString(EXTRA_POST_ID, postModel.getId());
            args.putString(EXTRA_POST_MESSAGE, postModel.getMessage());

            if (!TextUtils.isEmpty(postModel.getFullPicture())) {
                args.putString(EXTRA_POST_IMAGE, postModel.getFullPicture());
            }
            /*if (postModel.getAttachments() != null) {
                List<AttachmentModel> attachmentModelList = postModel.getAttachments().getData();
                if (attachmentModelList != null && !attachmentModelList.isEmpty()) {
                    args.putString(EXTRA_IMAGE, attachmentModelList.get(0).getUrl());
                }
            }*/
            dialogFragment.setArguments(args);
        }
        dialogFragment.show(activity.getFragmentManager(), NewPostDialogFragment.class.getSimpleName());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, 0);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.new_post, container, false);

        Toolbar toolbar = (Toolbar)view.findViewById(R.id.toolbar);
        toolbar.getMenu()
                .add(R.string.send_post_button_label)
                .setIcon(R.drawable.ic_action_send)
                .setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_ALWAYS)
                .setOnMenuItemClickListener(menuItem -> {
                    sendPost();
                    return true;
                });

        mPostText = (EditText)view.findViewById(R.id.post_text);
        mPostImageProgress = (ProgressBar)view.findViewById(R.id.post_image_progress);
        mPostImageProgress.setVisibility(View.INVISIBLE);
        mPostImageLabel = (TextView)view.findViewById(R.id.post_image_label);
        mPostImageLabel.setVisibility(View.VISIBLE);
        mPostImage = (ImageView)view.findViewById(R.id.post_image);
        mPostImage.setOnClickListener(this);

        mPresenter = new NewPostPresenterImpl(getActivity(), this);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle args = getArguments();
        if (args != null) {
            setPostId(args.getString(EXTRA_POST_ID, null));
            setPostMessage(args.getString(EXTRA_POST_MESSAGE, ""));
            setPostImage(args.getString(EXTRA_POST_IMAGE));

        } else if (savedInstanceState != null) {
            setPostImage(savedInstanceState.get(EXTRA_POST_IMAGE));
        }
        loadImage();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.post_image:
                selectImageFile(true);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    selectImageFile(false);

                } else {
                    // TODO: show dialog
                }
            break;

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CODE_SELECT_IMAGE_FILE:
                if (resultCode == Activity.RESULT_OK && data != null) {
                    Uri uri = data.getData();
                    setPostImage(uri);
                    mPresenter.uploadImage(uri);
                }
                break;

            default:
                super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        Object tag = getPostImage();
        if (tag == null) {
            return;
        }

        if (tag instanceof Uri) {
            outState.putParcelable(EXTRA_POST_IMAGE, (Uri) tag);
        } else if (tag instanceof String) {
            outState.putString(EXTRA_POST_IMAGE, (String) tag);
        }
    }

    @Override
    public void onDestroyView() {
        if (mPresenter != null) {
            mPresenter.destroy();
            mPresenter = null;
        }
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
        super.onDestroyView();
    }

    @Override
    public void showProgress() {
        mProgressDialog = ProgressDialog
                .show(getActivity(), null, getString(R.string.loading_dialog_message), true, false);
    }

    @Override
    public void hideProgress() {
        if (mProgressDialog != null) {
            mProgressDialog.hide();
            mProgressDialog = null;
        }
    }

    @Override
    public void showUploadImageProgress() {
        mPostImageProgress.setVisibility(View.VISIBLE);
        mPostImageLabel.setVisibility(View.INVISIBLE);
        mPostImage.setImageDrawable(null);
    }

    @Override
    public void hideUploadImageProgress() {
        mPostImageProgress.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onUploadImage(AttachedMedia attachedMedia) {

        if (mAttachedMediaList == null) {
            mAttachedMediaList = new ArrayList<>();
        }
        // TODO: only one image
        mAttachedMediaList.clear();
        mAttachedMediaList.add(attachedMedia);

        loadImage();
    }

    @Override
    public void onSendPostSuccess(@Nullable PostModel postModel) {
        PostListFragment.sendActionUpdate(getActivity(), postModel);
        getFragmentManager().beginTransaction().remove(this).commit();
    }

    @Override
    public void onErrorMessage(String msg) {
        mPostImageLabel.setVisibility(View.VISIBLE);
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
    }

    private void sendPost() {

        String message = getPostMessage();
        if (TextUtils.isEmpty(message)) {
            return;
        }

        PostRequest postRequest = new PostRequest.Builder()
                .withMessage(message)
                .withAttachedMediaList(mAttachedMediaList)
                .build();

        String postId = getPostId();
        if (TextUtils.isEmpty(postId)) {
            mPresenter.sendPost(postRequest);
        } else {
            mPresenter.updatePost(postId, postRequest);
        }
    }

    @SuppressLint("NewApi")
    private void selectImageFile(boolean checkPermission) {

        if (checkPermission && Utils.isAPI23plus() &&
                getActivity().checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_CODE_READ_EXTERNAL_STORAGE);
            return;
        }

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        intent.setType("image/*");

        if (!Utils.isAPI19plus()) {
            intent = Intent.createChooser(intent, getString(R.string.choise_application_label));
        }

        startActivityForResult(intent, REQUEST_CODE_SELECT_IMAGE_FILE);
    }

    @Nullable
    private String getPostId() {
        return (String) mPostText.getTag();
    }

    private void setPostId(@Nullable String id) {
        mPostText.setTag(id);
    }

    @NonNull
    private String getPostMessage() {
        return mPostText.getText().toString().trim();
    }

    private void setPostMessage(@NonNull String message) {
        mPostText.setText(message);
        mPostText.setSelection(message.length());
    }

    @Nullable
    private Object getPostImage() {
        return mPostImage.getTag();
    }

    private void setPostImage(@Nullable Object postImage) {
        mPostImage.setTag(postImage);
    }

    private void loadImage() {

        Object postImage = getPostImage();
        if (postImage == null) {
            return;
        }

        mPostImageLabel.setVisibility(View.INVISIBLE);

        if (postImage instanceof Uri) {
            Picasso.with(getActivity())
                    .load((Uri) postImage)
                    .into(mPostImage);
        } else if (postImage instanceof String) {
            Picasso.with(getActivity())
                    .load((String) postImage)
                    .into(mPostImage);
        }
    }

}
