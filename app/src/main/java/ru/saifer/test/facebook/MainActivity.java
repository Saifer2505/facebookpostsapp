package ru.saifer.test.facebook;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import ru.saifer.test.facebook.api.models.PostModel;
import ru.saifer.test.facebook.core.BaseActivity;
import ru.saifer.test.facebook.screens.newpost.view.NewPostDialogFragment;
import ru.saifer.test.facebook.screens.posts.view.PostListFragment;

public class MainActivity extends BaseActivity {

    private Toolbar mToolbar;
    private CallbackManager mCallbackManager;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        mCallbackManager = CallbackManager.Factory.create();

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        final ProgressBar progress = (ProgressBar)findViewById(R.id.main_progress);
        progress.setVisibility(View.INVISIBLE);

        final LoginButton loginButton = (LoginButton)findViewById(R.id.login_button);
        loginButton.setVisibility(View.INVISIBLE);
        loginButton.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                loginButton.setVisibility(View.INVISIBLE);
                startPostListFragment(MainActivity.this);
            }

            @Override
            public void onCancel() {
            }

            @Override
            public void onError(FacebookException e) {
                e.printStackTrace();
                Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        if (accessToken == null) {
            loginButton.setVisibility(View.VISIBLE);
            return;
        }

        if (accessToken.isExpired()) {

            progress.setVisibility(View.VISIBLE);
            AccessToken.refreshCurrentAccessTokenAsync(new AccessToken.AccessTokenRefreshCallback() {
                @Override
                public void OnTokenRefreshed(AccessToken accessToken) {
                    progress.setVisibility(View.INVISIBLE);
                    if (savedInstanceState == null) {
                        startPostListFragment(MainActivity.this);
                    }
                }

                @Override
                public void OnTokenRefreshFailed(FacebookException e) {
                    e.printStackTrace();

                    progress.setVisibility(View.INVISIBLE);
                    loginButton.setVisibility(View.VISIBLE);

                    LoginManager.getInstance().logOut();
                }
            });
        } else if (savedInstanceState == null) {
            startPostListFragment(this);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (mCallbackManager != null) {
            mCallbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void setTitle(CharSequence title) {
        super.setTitle(title);
        mToolbar.setTitle(title);
    }

    public static void logout(Activity activity) {
        LoginManager.getInstance().logOut();
        AccessToken.setCurrentAccessToken(null);

        if (activity != null) {
            View loginButton = activity.findViewById(R.id.login_button);
            loginButton.setVisibility(View.VISIBLE);
        }
    }

    private static void startPostListFragment(Activity activity) {
        activity.getFragmentManager()
                .beginTransaction()
                .replace(R.id.container, new PostListFragment(), PostListFragment.class.getSimpleName())
                .commit();
    }

    public static void startNewPostDialogFragment(Activity activity) {
        startNewPostDialogFragment(activity, null);
    }

    public static void startNewPostDialogFragment(Activity activity, PostModel postModel) {
        NewPostDialogFragment.start(activity, postModel);

        /*activity.getFragmentManager()
                .beginTransaction()
                .addToBackStack(NewPostDialogFragment.class.getSimpleName())
                .replace(R.id.container, new NewPostDialogFragment())
                .commit();*/
    }

}
