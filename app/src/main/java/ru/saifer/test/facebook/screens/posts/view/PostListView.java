package ru.saifer.test.facebook.screens.posts.view;

import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;

import java.util.List;

import ru.saifer.test.facebook.api.models.PostModel;
import ru.saifer.test.facebook.core.BaseView;

public interface PostListView extends BaseView {

    @Nullable
    RecyclerView getRecyclerView();
    void onPostData(@Nullable List<PostModel> data, boolean isOffset);
    void onDeletePostFailed(int position, @NonNull PostModel postModel);
    void onSetClickListenerByLogoutDialog(@NonNull DialogInterface.OnClickListener listener);
    void onLogout();
}
