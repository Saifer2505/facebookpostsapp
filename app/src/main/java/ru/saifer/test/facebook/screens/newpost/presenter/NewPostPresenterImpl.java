package ru.saifer.test.facebook.screens.newpost.presenter;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.io.File;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import ru.saifer.test.facebook.R;
import ru.saifer.test.facebook.api.ServerFactory;
import ru.saifer.test.facebook.api.models.PostModel;
import ru.saifer.test.facebook.api.models.requests.AttachedMedia;
import ru.saifer.test.facebook.api.models.requests.BaseRequest;
import ru.saifer.test.facebook.api.models.requests.PostRequest;
import ru.saifer.test.facebook.core.BasePresenterImpl;
import ru.saifer.test.facebook.db.DBFactory;
import ru.saifer.test.facebook.screens.newpost.view.NewPostView;
import ru.saifer.test.facebook.utils.Utils;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class NewPostPresenterImpl extends BasePresenterImpl implements NewPostPresenter {

    public NewPostPresenterImpl(Context context, NewPostView view) {
        super(context, view);
    }

    @NonNull
    @Override
    protected NewPostView getView() {
        return (NewPostView) super.getView();
    }

    @Override
    public void uploadImage(@Nullable Uri imageUri) {

        if (imageUri == null) {
            getView().onErrorMessage(getContext().getString(R.string.file_not_found_label));
            return;
        }

        String path = Utils.getPath(getContext(), imageUri);
        if (path == null) {
            getView().onErrorMessage(getContext().getString(R.string.file_not_found_label));
            return;
        }

        getView().showUploadImageProgress();

        File file = new File(path);

        RequestBody requestFile = RequestBody.create(
                MediaType.parse(getContext().getContentResolver().getType(imageUri)), file);

        MultipartBody.Part tokenPart = MultipartBody.Part
                .createFormData("access_token", BaseRequest.getAccessToken());

        MultipartBody.Part publishedPart = MultipartBody.Part
                .createFormData("published", "false");

        MultipartBody.Part filePart = MultipartBody.Part
                .createFormData("source", file.getName(), requestFile);

        setSubscription(ServerFactory.getInstance().photo(tokenPart, publishedPart, filePart)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(struct -> {
                    getView().hideUploadImageProgress();
                    getView().onUploadImage(new AttachedMedia(struct));

                }, throwable -> {
                    throwable.printStackTrace();
                    getView().hideUploadImageProgress();
                    getView().onErrorMessage(throwable.getMessage());
                }));
    }

    @Override
    public void sendPost(@NonNull PostRequest postRequest) {
        getView().showProgress();

        setSubscription(ServerFactory.getInstance().publishPost(postRequest.toMap())
                .switchMap(struct -> getPostModelObservable(struct.getId()))
                .doOnNext(DBFactory::savePost)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(postModel -> {
                    getView().hideProgress();
                    getView().onSendPostSuccess(postModel);

                }, throwable -> {
                    throwable.printStackTrace();
                    getView().hideProgress();
                    getView().onErrorMessage(throwable.getMessage());
                }));
    }

    @Override
    public void updatePost(@NonNull final String postId, @NonNull PostRequest postRequest) {
        getView().showProgress();

        setSubscription(ServerFactory.getInstance().updatePost(postId, postRequest.toMap())
                .switchMap(struct -> getPostModelObservable(postId))
                .doOnNext(DBFactory::savePost)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(postModel -> {
                    getView().hideProgress();
                    getView().onSendPostSuccess(postModel);

                }, throwable -> {
                    throwable.printStackTrace();
                    getView().hideProgress();
                    getView().onErrorMessage(throwable.getMessage());
                }));
    }

    @NonNull
    private Observable<PostModel> getPostModelObservable(@NonNull String postId) {

        Map<String, String> query = new BaseRequest().toMap();
        query.put("fields", "id,created_time,updated_time,from,message,picture,full_picture,type,attachments");

        return ServerFactory.getInstance().getPost(postId, query);
    }

}
