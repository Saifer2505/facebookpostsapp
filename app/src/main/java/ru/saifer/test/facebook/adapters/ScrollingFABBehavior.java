package ru.saifer.test.facebook.adapters;

import android.content.Context;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.AttributeSet;
import android.view.View;

import ru.saifer.test.facebook.utils.Utils;

public class ScrollingFABBehavior extends CoordinatorLayout.Behavior<FloatingActionButton> {

    private int mToolbarHeight;

    public ScrollingFABBehavior(Context context, AttributeSet attrs) {
        super(context, attrs);
        mToolbarHeight = Utils.getToolbarHeight(context);
    }

    @Override
    public boolean layoutDependsOn(CoordinatorLayout parent, FloatingActionButton fab, View dependency) {
        return dependency instanceof AppBarLayout || dependency instanceof Snackbar.SnackbarLayout;
    }

    @Override
    public boolean onDependentViewChanged(CoordinatorLayout parent, FloatingActionButton fab, View dependency) {
        if (dependency instanceof AppBarLayout) {
            CoordinatorLayout.LayoutParams lp = (CoordinatorLayout.LayoutParams) fab.getLayoutParams();
            int fabBottomMargin = lp.bottomMargin;
            int distanceToScroll = fab.getHeight() + fabBottomMargin;
            float ratio = (float)dependency.getY()/(float)mToolbarHeight;
            fab.setTranslationY(-distanceToScroll * ratio);

        } else if (dependency instanceof Snackbar.SnackbarLayout) {
            fab.setTranslationY(Math.min(0, dependency.getTranslationY() - dependency.getHeight()));
        }
        return true;
    }
}
