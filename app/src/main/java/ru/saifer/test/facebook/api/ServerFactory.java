package ru.saifer.test.facebook.api;

import android.support.annotation.NonNull;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.saifer.test.facebook.BuildConfig;

public class ServerFactory {

    private static FacebookApi mFacebookApi;

    @NonNull
    public static FacebookApi getInstance() {
        if (mFacebookApi == null) {

            Retrofit.Builder retrofitBuilder = new Retrofit.Builder()
                    .baseUrl(FacebookApi.API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create());

            if (BuildConfig.DEBUG) {
                HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
                interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
                retrofitBuilder.client(new OkHttpClient.Builder().addInterceptor(interceptor).build());
            }

            mFacebookApi = retrofitBuilder.build().create(FacebookApi.class);
        }
        return mFacebookApi;
    }

}
