package ru.saifer.test.facebook.api.models.requests;

import android.support.annotation.NonNull;

import com.facebook.AccessToken;

import java.util.HashMap;
import java.util.Map;

public class BaseRequest {

    @NonNull
    public static String getAccessToken() {
        AccessToken token = AccessToken.getCurrentAccessToken();
        return token == null ? "" : token.getToken();
    }

    public Map<String, String> toMap() {
        HashMap<String, String> hm = new HashMap<>();
        hm.put("access_token", getAccessToken());
        return hm;
    }
}
