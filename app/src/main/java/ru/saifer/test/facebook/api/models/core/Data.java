package ru.saifer.test.facebook.api.models.core;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Data<T> implements Serializable {

    @SerializedName("data")
    @Expose
    private List<T> data;

    @SerializedName("paging")
    @Expose
    private Paging paging;

    public Data(List<T> data) {
        this.data = data;
    }

    public List<T> getData() {
        return data;
    }
    public void setData(List<T> data) {
        this.data = data;
    }

    public Paging getPaging() {
        return paging;
    }
    public void setPaging(Paging paging) {
        this.paging = paging;
    }
}
