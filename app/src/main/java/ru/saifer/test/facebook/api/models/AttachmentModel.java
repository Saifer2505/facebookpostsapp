package ru.saifer.test.facebook.api.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import ru.saifer.test.facebook.api.models.core.Data;

public class AttachmentModel implements Serializable {

    private static final String TYPE_PHOTO = "photo";
    private static final String TYPE_ALBUM = "album";
    private static final String TYPE_VIDEO_AUTOPLAY = "video_autoplay";

    /* Album Type */
    @SerializedName("subattachments")
    @Expose
    private Data<AttachmentModel> subattachments;
    @SerializedName("title")
    @Expose
    private String title;

    /* Photo Type */
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("media")
    @Expose
    private MediaModel media;

    //@SerializedName("target")
    //@Expose
    //private Target target;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("url")
    @Expose
    private String url;

    public Data<AttachmentModel> getSubattachments() {
        return subattachments;
    }
    public void setSubattachmentsData(Data<AttachmentModel> subattachments) {
        this.subattachments = subattachments;
    }

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public MediaModel getMedia() {
        return media;
    }
    public void setMedia(MediaModel media) {
        this.media = media;
    }

    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }
    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isAlbumType() {
        return TYPE_ALBUM.equalsIgnoreCase(type);
    }

}
