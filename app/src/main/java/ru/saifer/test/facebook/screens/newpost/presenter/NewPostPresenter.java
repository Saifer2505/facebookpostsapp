package ru.saifer.test.facebook.screens.newpost.presenter;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import ru.saifer.test.facebook.api.models.requests.PostRequest;
import ru.saifer.test.facebook.core.BasePresenter;

public interface NewPostPresenter extends BasePresenter {

    void uploadImage(@Nullable Uri imageUri);
    void sendPost(@NonNull PostRequest postRequest);
    void updatePost(@NonNull String postId, @NonNull PostRequest postRequest);

}
