package ru.saifer.test.facebook.screens.posts.presenter;

import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import java.util.List;
import java.util.Map;

import ru.saifer.test.facebook.Const;
import ru.saifer.test.facebook.api.ServerFactory;
import ru.saifer.test.facebook.api.models.PostModel;
import ru.saifer.test.facebook.api.models.core.Data;
import ru.saifer.test.facebook.api.models.core.Paging;
import ru.saifer.test.facebook.api.models.requests.BaseRequest;
import ru.saifer.test.facebook.core.BasePresenterImpl;
import ru.saifer.test.facebook.db.DBFactory;
import ru.saifer.test.facebook.screens.posts.paging.PaginationTool;
import ru.saifer.test.facebook.screens.posts.view.PostListView;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class PostListPresenterImpl extends BasePresenterImpl implements PostListPresenter {

    private Subscription mPagingSubscription;

    public PostListPresenterImpl(Context context, PostListView view) {
        super(context, view);

        mPagingSubscription = getRecyclerViewPagingSubscription();
    }

    @NonNull
    @Override
    protected PostListView getView() {
        return (PostListView) super.getView();
    }

    @Override
    public void destroy() {
        super.destroy();

        if (mPagingSubscription != null && !mPagingSubscription.isUnsubscribed()) {
            mPagingSubscription.unsubscribe();
            mPagingSubscription = null;
        }
    }

    @Override
    public void logout() {
        getView().onSetClickListenerByLogoutDialog((dialogInterface, i) -> {
            dialogInterface.dismiss();
            if (i == DialogInterface.BUTTON_POSITIVE) {
                setSubscription(getClearPostsObservable().subscribe(o -> getView().onLogout()));
            }
        });
    }

    @Override
    public void getPosts() {
        //System.out.println(PostListPresenterImpl.class.getSimpleName().concat(".getPosts()"));

        getView().showProgress();

        setSubscription(getNetworkObservable(null)
                .last()
                .switchMap(postModelData -> getDiskObservable(0))
                .onErrorResumeNext(throwable -> {
                    throwable.printStackTrace();
                    return getDiskObservable(0);
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(postModels -> {
                    getView().hideProgress();
                    getView().onPostData(postModels, false);
                }));
    }

    @Override
    public void syncPosts() {
        //System.out.println(PostListPresenterImpl.class.getSimpleName().concat(".syncPosts()"));

        setSubscription(getNetworkObservable(null)
                .map(Data::getData)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    data -> getView().onPostData(data, false),
                    throwable -> {
                        throwable.printStackTrace();
                        getView().onErrorMessage(throwable.getMessage());
                    }));
    }

    @Override
    public void clearPosts() {
        //System.out.println(PostListPresenterImpl.class.getSimpleName().concat(".clearPosts()"));

        setSubscription(getClearPostsObservable().subscribe(b -> getView().hideProgress()));
    }

    @Override
    public void deletePost(final int position, final @NonNull PostModel postModel) {

        Map<String, String> query = new BaseRequest().toMap();

        setSubscription(ServerFactory.getInstance().deletePost(postModel.getId(), query)
                .doOnNext(struct -> DBFactory.removePost(postModel.getId()))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(struct -> {
                }, throwable -> {
                    throwable.printStackTrace();
                    getView().onErrorMessage(throwable.getMessage());
                    getView().onDeletePostFailed(position, postModel);
                }));
    }

    @NonNull
    private Observable<Boolean> getClearPostsObservable() {
        return Observable.just(false)
                .doOnCompleted(DBFactory::clearDB)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @NonNull
    private Observable<List<PostModel>> getDiskObservable(int offset) {
        return Observable.just(DBFactory.getPostModelList(offset, Const.POSTS_LIMIT_COUNT));
    }

    @NonNull
    private Observable<Data<PostModel>> getNetworkObservable(@Nullable String url) {

        Observable<Data<PostModel>> postModelDataObservable;

        if (TextUtils.isEmpty(url)) {
            Map<String, String> query = new BaseRequest().toMap();
            query.put("fields", "id,created_time,updated_time,from,message,picture,full_picture,type,attachments");
            query.put("limit", String.valueOf(Const.POSTS_LIMIT_COUNT));

            long lastCreatedTime = DBFactory.getLastPostCreatedTime() / 1000L;
            if (lastCreatedTime > 0) {
                query.put("since", String.valueOf(lastCreatedTime));
            }
            postModelDataObservable = ServerFactory.getInstance().getPosts(query);
        } else {
            postModelDataObservable = ServerFactory.getInstance().getPosts(url);
        }

        return postModelDataObservable
                .subscribeOn(Schedulers.io())
                .doOnNext(postModelData -> DBFactory.savePostList(postModelData.getData()))
                .concatMap(postModelData -> {
                    Paging paging = postModelData.getPaging();
                    if (paging != null && !TextUtils.isEmpty(paging.getNext())) {
                        return Observable.just(postModelData)
                                .subscribeOn(Schedulers.io())
                                .concatWith(getNetworkObservable(paging.getNext()));
                    }
                    return Observable.just(postModelData);
                });
    }

    @Nullable
    private Subscription getRecyclerViewPagingSubscription() {
        return getView().getRecyclerView() == null ? null :
                PaginationTool.paging(getView().getRecyclerView(), this::getDiskObservable, Const.POSTS_LIMIT_COUNT)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Subscriber<List<PostModel>>() {
                            @Override
                            public void onCompleted() {
                            }
                            @Override
                            public void onError(Throwable e) {
                                e.printStackTrace();
                                getView().onErrorMessage(e.getMessage());
                            }
                            @Override
                            public void onNext(List<PostModel> postModelList) {
                                getView().onPostData(postModelList, true);
                            }
                        });
    }

}
