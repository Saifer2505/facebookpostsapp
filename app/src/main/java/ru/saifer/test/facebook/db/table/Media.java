package ru.saifer.test.facebook.db.table;

import android.os.Parcel;
import android.os.Parcelable;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

@Table(name="Media", id = "_id")
public class Media extends Model implements Parcelable {

    @Column(name = "image_id")
    public Image image;

    public static final Parcelable.Creator<Media> CREATOR = new Parcelable.Creator<Media>() {
        public Media createFromParcel(Parcel in) {
            return new Media(in);
        }
        public Media[] newArray(int size) {
            return new Media[size];
        }
    };

    public Media() {
    }

    private Media(Parcel paramParcel) {
        image = paramParcel.readParcelable(Image.class.getClassLoader());
    }

    public void writeToParcel(Parcel paramParcel, int paramInt) {
        paramParcel.writeParcelable(image, paramInt);
    }

    public int describeContents() {
        return 0;
    }

    @Override
    public String toString() {
        return "["+
                "image=" + image +
                "]";
    }

}
