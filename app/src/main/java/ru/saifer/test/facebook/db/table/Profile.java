package ru.saifer.test.facebook.db.table;

import android.os.Parcel;
import android.os.Parcelable;

import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import ru.saifer.test.facebook.db.core.DataBaseModel;

@Table(name="Profiles", id = "_id")
public class Profile extends DataBaseModel {

    public static final Parcelable.Creator<Profile> CREATOR = new Parcelable.Creator<Profile>() {
        public Profile createFromParcel(Parcel in) {
            return new Profile(in);
        }
        public Profile[] newArray(int size) {
            return new Profile[size];
        }
    };

    @Column
    public String name;

    public Profile() {
        super();
    }

    private Profile(Parcel paramParcel) {
        super(paramParcel);
        name = paramParcel.readString();
    }

    public void writeToParcel(Parcel paramParcel, int paramInt) {
        super.writeToParcel(paramParcel, paramInt);
        paramParcel.writeString(name);
    }

    public int describeContents() {
        return 0;
    }

    @Override
    public String toString() {
        return "["+
                "id=" + id +
                ", name=" + name +
                "]";
    }
}
