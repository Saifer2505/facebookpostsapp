package ru.saifer.test.facebook.screens.posts.paging;

import java.util.List;

import rx.Observable;

public interface PagingListener<T> {

    Observable<List<T>> onNextPage(int offset);
}
