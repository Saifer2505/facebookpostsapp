package ru.saifer.test.facebook.db.table;

import android.os.Parcel;
import android.os.Parcelable;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

@Table(name="Images", id = "_id")
public class Image extends Model implements Parcelable {

    public static final Parcelable.Creator<Image> CREATOR = new Parcelable.Creator<Image>() {
        public Image createFromParcel(Parcel in) {
            return new Image(in);
        }
        public Image[] newArray(int size) {
            return new Image[size];
        }
    };

    @Column
    public String src;
    @Column
    public int width;
    @Column
    public int height;

    public Image() {
    }

    private Image(Parcel paramParcel) {
        src = paramParcel.readString();
        width = paramParcel.readInt();
        height = paramParcel.readInt();
    }

    public void writeToParcel(Parcel paramParcel, int paramInt) {
        paramParcel.writeString(src);
        paramParcel.writeInt(width);
        paramParcel.writeInt(height);
    }

    public int describeContents() {
        return 0;
    }

    @Override
    public String toString() {
        return "["+
                "src=" + src +
                ", width=" + width +
                ", height=" + height +
                "]";
    }
}
