package ru.saifer.test.facebook.screens.posts.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import ru.saifer.test.facebook.R;
import ru.saifer.test.facebook.api.models.PostModel;
import ru.saifer.test.facebook.utils.StringUtils;
import rx.Observable;
import rx.subjects.PublishSubject;

public class PostListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_ITEM = 1;

    private Context mContext;
    private List<PostModel> mDataset;
    private final PublishSubject<PostModel> mOnClickItemSubject = PublishSubject.create();

    private static class RecyclerItemViewHolder extends RecyclerView.ViewHolder {

        private TextView userName;
        private TextView dateTime;
        private TextView message;
        private ImageView image;

        private RecyclerItemViewHolder(View v) {
            super(v);
            userName = (TextView)v.findViewById(R.id.user_name);
            dateTime = (TextView)v.findViewById(R.id.date_time);
            message = (TextView)v.findViewById(R.id.message);
            image = (ImageView)v.findViewById(R.id.image);
        }
    }

    public PostListAdapter(Context context) {
        mContext = context;
        mDataset = new ArrayList<>();
    }

    private PostModel getItem(int position) {
        return mDataset.get(position);
    }

    public void addItem(int i, PostModel postModel) {
        mDataset.add(i, postModel);
        notifyItemInserted(i);

        // Добавлен костыль, т.к. первый элемент не видно
        if (i == 0) {
            mDataset.add(1, postModel);
            notifyItemInserted(1);
            mDataset.remove(0);
            notifyDataSetChanged();
        }
    }

    public void addItems(List<PostModel> postModelList, boolean isInEnd) {

        if (postModelList == null) {
            return;
        }

        if (isInEnd) {
            mDataset.addAll(postModelList);
            notifyItemRangeInserted(getItemCount() - 1, postModelList.size());
        } else {
            mDataset.addAll(0, postModelList);
            notifyItemRangeInserted(0, postModelList.size());
        }
    }

    public void addOrUpdateItem(PostModel postModel) {
        int i = 0;
        for (PostModel oldPostModel: mDataset) {
            if (oldPostModel.getId().equals(postModel.getId())) {
                oldPostModel.set(postModel);
                notifyItemChanged(i);
                return;
            }
            i++;
        }
        addItem(0, postModel);
    }

    public PostModel removeItem(int position) {
        PostModel postModel = getItem(position);
        mDataset.remove(position);
        notifyItemRemoved(position);
        return postModel;
    }

    public void clear() {
        mDataset.clear();
        notifyDataSetChanged();
    }

    public Observable<PostModel> getOnClickItemObservable(){
        return mOnClickItemSubject.asObservable();
    }

    public boolean isEmpty() {
        return getItemCount() == 0;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.post_list_item, parent, false);
            return new RecyclerItemViewHolder(view);
        }
        throw new RuntimeException("There is no type that matches the type " + viewType + ", make sure your using types correctly");
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        PostModel postModel = getItem(position);
        RecyclerItemViewHolder itemViewHolder = (RecyclerItemViewHolder)holder;
        itemViewHolder.itemView.setOnClickListener(view -> mOnClickItemSubject.onNext(postModel));
        itemViewHolder.userName.setText(postModel.getFrom().getName());
        itemViewHolder.dateTime.setText(StringUtils.parseDate(postModel.getCreatedTime(), "dd.MM.yyyy HH:mm"));
        itemViewHolder.message.setText(postModel.getMessage());

        if (TextUtils.isEmpty(postModel.getFullPicture())) {
            itemViewHolder.image.setVisibility(View.GONE);
        } else {
            itemViewHolder.image.setVisibility(View.VISIBLE);
            Picasso.with(mContext)
                    .load(postModel.getFullPicture())
                    .placeholder(R.drawable.ic_attach_file)
                    .into(itemViewHolder.image);
        }
    }

    @Override
    public int getItemCount() {
        return mDataset == null ? 0 : mDataset.size();
    }

    @Override
    public int getItemViewType(int position) {
        return TYPE_ITEM;
    }
}
