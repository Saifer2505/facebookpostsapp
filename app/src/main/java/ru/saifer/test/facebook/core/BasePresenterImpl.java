package ru.saifer.test.facebook.core;

import android.content.Context;
import android.support.annotation.NonNull;

import rx.Subscription;

public class BasePresenterImpl implements BasePresenter {

    @NonNull
    private Context mContext;
    @NonNull
    private BaseView mView;
    private Subscription mSubscription;

    protected BasePresenterImpl(@NonNull Context context, @NonNull BaseView view) {
        mContext = context;
        mView = view;
    }

    @NonNull
    protected Context getContext() {
        return mContext;
    }

    @NonNull
    protected BaseView getView() {
        return mView;
    }

    @Override
    public void destroy() {
        unsubscribe();
    }

    protected void setSubscription(Subscription subscription) {
        unsubscribe();
        mSubscription = subscription;
    }

    private void unsubscribe() {
        if (mSubscription != null && !mSubscription.isUnsubscribed()) {
            mSubscription.unsubscribe();
            mSubscription = null;
        }
    }
}
