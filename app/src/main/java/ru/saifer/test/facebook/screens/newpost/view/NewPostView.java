package ru.saifer.test.facebook.screens.newpost.view;

import android.support.annotation.Nullable;

import ru.saifer.test.facebook.api.models.PostModel;
import ru.saifer.test.facebook.api.models.requests.AttachedMedia;
import ru.saifer.test.facebook.core.BaseView;

public interface NewPostView extends BaseView {

    void showUploadImageProgress();
    void hideUploadImageProgress();
    void onUploadImage(AttachedMedia attachedMedia);
    void onSendPostSuccess(@Nullable PostModel postModel);
}
