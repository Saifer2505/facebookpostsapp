package ru.saifer.test.facebook.screens.posts.presenter;

import android.support.annotation.NonNull;

import ru.saifer.test.facebook.api.models.PostModel;
import ru.saifer.test.facebook.core.BasePresenter;

public interface PostListPresenter extends BasePresenter {

    void logout();
    void getPosts();
    void syncPosts();
    void clearPosts();
    void deletePost(int position, @NonNull PostModel postModel);
}
