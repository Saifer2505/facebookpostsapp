package ru.saifer.test.facebook.db;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;

import java.util.ArrayList;
import java.util.List;

import ru.saifer.test.facebook.api.models.AttachmentModel;
import ru.saifer.test.facebook.api.models.ImageModel;
import ru.saifer.test.facebook.api.models.MediaModel;
import ru.saifer.test.facebook.api.models.PostModel;
import ru.saifer.test.facebook.api.models.ProfileModel;
import ru.saifer.test.facebook.api.models.core.Data;
import ru.saifer.test.facebook.db.table.Attachment;
import ru.saifer.test.facebook.db.table.Image;
import ru.saifer.test.facebook.db.table.Media;
import ru.saifer.test.facebook.db.table.Post;
import ru.saifer.test.facebook.db.table.Profile;

public class DBFactory {

    public static void clearDB() {
        new Delete().from(Attachment.class).execute();
        new Delete().from(Post.class).execute();
        new Delete().from(Profile.class).execute();
        new Delete().from(Media.class).execute();
        new Delete().from(Image.class).execute();
    }

    public static void removePost(@NonNull String postId) {
        new Delete().from(Post.class).where("id = ?", postId).execute();
    }

    @Nullable
    public static Post getPost(String postId) {
        return new Select().from(Post.class).where("id = ?", postId).executeSingle();
    }

    @NonNull
    public static List<Post> getPostList(int offset, int limit) {
        return new Select().from(Post.class).orderBy("created_time DESC").offset(offset).limit(limit).execute();
    }

    @NonNull
    public static List<PostModel> getPostModelList(int offset, int limit) {
        List<Post> postList = getPostList(offset, limit);
        List<PostModel> postModelList = new ArrayList<>(postList.size());
        for (Post post: postList) {
            postModelList.add(post.toPostModel());
        }
        return postModelList;
    }

    public static long getLastPostCreatedTime() {
        Post post = new Select().from(Post.class).orderBy("created_time DESC").executeSingle();
        return post != null ? post.createdTime.getTime() : -1L;
    }

    @NonNull
    public static List<Attachment> getAttachmentListByPost(@NonNull Post post) {
        return new Select().from(Attachment.class).where("post_id = ?", post.getId()).execute();
    }

    public static void savePostList(@NonNull List<PostModel> postModelList) {
        ActiveAndroid.beginTransaction();
        try {
            for (PostModel postModel: postModelList) {
                savePost(postModel);
            }
            ActiveAndroid.setTransactionSuccessful();
        } finally {
            ActiveAndroid.endTransaction();
        }
    }

    @NonNull
    public static Post savePost(@NonNull PostModel postModel) {

        Post post = getPost(postModel.getId());
        if (post == null) {
            post = new Post();
        }

        post.id = postModel.getId();
        post.createdTime = postModel.getCreatedTime();
        post.updatedTime = postModel.getUpdatedTime();
        post.from = saveProfile(postModel.getFrom());
        post.message = postModel.getMessage();
        post.picture = postModel.getPicture();
        post.fullPicture = postModel.getFullPicture();
        post.type = postModel.getType();
        post.save();

        saveAttachmentData(post, postModel.getAttachments());
        return post;
    }

    @Nullable
    private static Profile getProfile(@NonNull String profileId) {
        return new Select().from(Profile.class).where("id = ?", profileId).executeSingle();
    }

    @NonNull
    private static Profile saveProfile(@NonNull ProfileModel profileModel) {

        Profile profile = getProfile(profileModel.getId());
        if (profile == null) {
            profile = new Profile();
        }

        profile.id = profileModel.getId();
        profile.name = profileModel.getName();
        profile.save();
        return profile;
    }

    @Nullable
    private static Attachment getAttachment(@NonNull Post post, @NonNull Media media) {
        return new Select().from(Attachment.class).where("post_id = ? AND media_id = ?", post.getId(), media.getId()).executeSingle();
    }

    private static void saveAttachmentData(@NonNull Post post, @Nullable Data<AttachmentModel> attachmentModelData) {

        if (attachmentModelData == null) {
            return;
        }

        List<AttachmentModel> attachmentModelList = attachmentModelData.getData();
        if (attachmentModelList == null || attachmentModelList.isEmpty()) {
            return;
        }

        if (attachmentModelList.get(0).isAlbumType()) {
            attachmentModelList = attachmentModelList.get(0).getSubattachments().getData();
        }

        for (AttachmentModel attachmentModel: attachmentModelList) {

            Media media = saveMedia(attachmentModel.getMedia());

            Attachment attachment = getAttachment(post, media);
            if (attachment == null) {
                attachment = new Attachment();
            }
            attachment.post = post;
            attachment.title = attachmentModel.getTitle();
            attachment.description = attachmentModel.getDescription();
            attachment.media = media;
            attachment.type = attachmentModel.getType();
            attachment.url = attachmentModel.getUrl();
            attachment.save();
        }
    }

    @Nullable
    private static Media getMedia(@NonNull Image image) {
        return new Select().from(Media.class).where("image_id = ?", image.getId()).executeSingle();
    }

    @NonNull
    private static Media saveMedia(@NonNull MediaModel mediaModel) {

        Image image = saveImage(mediaModel.getImage());

        Media media = getMedia(image);
        if (media == null) {
            media = new Media();
        }
        media.image = image;
        media.save();
        return media;
    }

    @Nullable
    private static Image getImage(@NonNull ImageModel imageModel) {
        return new Select().from(Image.class)
                .where("src = ? AND width = ? AND height = ?", imageModel.getSrc(), imageModel.getWidth(), imageModel.getHeight())
                .executeSingle();
    }

    @NonNull
    private static Image saveImage(@NonNull ImageModel imageModel) {

        Image image = getImage(imageModel);
        if (image == null) {
            image = new Image();
        }
        image.src = imageModel.getSrc();
        image.width = imageModel.getWidth();
        image.height = imageModel.getHeight();
        image.save();
        return image;
    }

}
