package ru.saifer.test.facebook.screens.posts.paging;

public class PagingException extends RuntimeException {

    public PagingException(String detailMessage) {
        super(detailMessage);
    }
}
