package ru.saifer.test.facebook.api.models.requests;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import ru.saifer.test.facebook.api.models.responses.Struct;

public class AttachedMedia {

    @SerializedName("media_fbid")
    @Expose
    private String mediaFbId;

    public AttachedMedia(Struct struct) {
        mediaFbId = struct.getId();
    }

    public String getMediaFbId() {
        return mediaFbId;
    }
    public void setMediaFbId(String mediaFbId) {
        this.mediaFbId = mediaFbId;
    }

    public String toJson() {
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        return gson.toJson(this);
    }
}
