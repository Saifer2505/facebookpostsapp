package ru.saifer.test.facebook.core;

public interface BaseView {

    void showProgress();
    void hideProgress();
    void onErrorMessage(String msg);
}
