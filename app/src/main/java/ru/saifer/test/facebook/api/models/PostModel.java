package ru.saifer.test.facebook.api.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;

import ru.saifer.test.facebook.api.models.core.BaseModel;
import ru.saifer.test.facebook.api.models.core.Data;

public class PostModel extends BaseModel implements Serializable {

    public static final String TYPE_STATUS = "status";
    public static final String TYPE_PHOTO = "photo";
    public static final String TYPE_VIDEO = "video";

    @SerializedName("created_time")
    @Expose
    private Date createdTime;
    @SerializedName("updated_time")
    @Expose
    private Date updatedTime;
    @SerializedName("from")
    @Expose
    private ProfileModel from;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("picture")
    @Expose
    private String picture;
    @SerializedName("full_picture")
    @Expose
    private String fullPicture;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("attachments")
    @Expose
    private Data<AttachmentModel> attachments;
    //application
    //instagram_eligibility
    //is_hidden
    //is_instagram_eligible
    //is_published
    //link
    //object_id
    //permalink_url
    //privacy
    //status_type

    public Date getCreatedTime() {
        return createdTime;
    }
    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Date getUpdatedTime() {
        return updatedTime;
    }
    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }

    public ProfileModel getFrom() {
        return from;
    }
    public void setFrom(ProfileModel from) {
        this.from = from;
    }

    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }

    public String getPicture() {
        return picture;
    }
    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getFullPicture() {
        return fullPicture;
    }
    public void setFullPicture(String fullPicture) {
        this.fullPicture = fullPicture;
    }

    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }

    public Data<AttachmentModel> getAttachments() {
        return attachments;
    }
    public void setAttachments(Data<AttachmentModel> attachments) {
        this.attachments = attachments;
    }

    public void set(PostModel postModel) {
        this.createdTime = postModel.createdTime;
        this.updatedTime = postModel.updatedTime;
        this.from = postModel.from;
        this.message = postModel.message;
        this.picture = postModel.picture;
        this.fullPicture = postModel.fullPicture;
        this.type = postModel.type;
        this.attachments = postModel.attachments;
    }

    @Override
    public String toString() {

        String fullPictureUrl = "";
        if (fullPicture != null) {
            fullPictureUrl = ", fullPicture="+fullPicture;
        }

        return "[message=" + message + fullPictureUrl+"]";
    }
}
