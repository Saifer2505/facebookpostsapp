package ru.saifer.test.facebook.api.models.requests;

import java.util.List;
import java.util.Map;

public class PostRequest extends BaseRequest {

    private String message;
    private List<AttachedMedia> attachedMediaList = null;

    public void setMessage(String message) {
        this.message = message;
    }

    public void setAttachedMediaList(List<AttachedMedia> attachedMediaList) {
        this.attachedMediaList = attachedMediaList;
    }

    public Map<String, String> toMap() {
        Map<String, String> hm = super.toMap();
        hm.put("message", message);
        if (attachedMediaList != null && !attachedMediaList.isEmpty()) {
            int i = 0;
            for(AttachedMedia attachedMedia: attachedMediaList) {
                hm.put("attached_media[".concat(String.valueOf(i)).concat("]"), attachedMedia.toJson());
                i++;
            }
        }
        return hm;
    }

    public static final class Builder {

        private PostRequest postRequest = new PostRequest();

        public Builder withMessage(String message) {
            postRequest.setMessage(message);
            return this;
        }

        public Builder withAttachedMediaList(List<AttachedMedia> attachedMediaList) {
            postRequest.setAttachedMediaList(attachedMediaList);
            return this;
        }

        public PostRequest build() {
            return postRequest;
        }
    }
}
