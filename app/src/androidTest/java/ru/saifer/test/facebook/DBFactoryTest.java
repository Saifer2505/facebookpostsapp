package ru.saifer.test.facebook;

import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import ru.saifer.test.facebook.db.DBFactory;
import ru.saifer.test.facebook.db.table.Attachment;
import ru.saifer.test.facebook.db.table.Post;

import static org.junit.Assert.assertNotNull;

@RunWith(AndroidJUnit4.class)
public class DBFactoryTest {

    @Test
    public void testGetPostList() {
        assertNotNull(getPostList());
    }

    private List<Post> getPostList() {
        List<Post> postList = DBFactory.getPostList(0, Const.POSTS_LIMIT_COUNT);

        for (Post post: postList) {
            System.out.println(post);

            List<Attachment> attachmentList = DBFactory.getAttachmentListByPost(post);
            if (!attachmentList.isEmpty()) {
                System.out.println("attachmentList[" + attachmentList.size() + "]");
            } else {
                System.out.println("attachmentList = null");
            }
        }
        return postList;
    }

    @Test
    public void testGetPost() {
        Post post = DBFactory.getPost("254941758274825_274213973014270");
        System.out.println(post);

        assertNotNull(post);
    }
}
